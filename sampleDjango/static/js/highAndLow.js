$(function() {
	/*変数定義*/
	var highandlowUrl = location.href;
	var mainpicUrl = highandlowUrl.replace("game/highandlow/", "");
	var coinaddUrl = highandlowUrl.replace("game/highandlow/", "coinadd/");
	var pattern = ["s","c","d","h"];
	var victory = 0;
	var nowptn = 0;
	var nownum = 0;
	var huseptn = 0;
	var husenum = 0;
	
	/*いらないボタンを隠す*/
	$("#highB").hide();
	$("#lowB").hide();
	$("#nextB").hide();
	$("#retryB").hide();
	$("#getB").hide();
	
	/*スタートボタン処理*/
	$("#startB").click(function(){
		/*現在カードを置く*/
		nowptn = pattern[Math.floor( Math.random() * 4 )];
		nownum = Math.floor( Math.random() * 13+1);
		
		$("#card1").attr('src', $("#card1").attr('src').replace('z02', nowptn + nownum));
		
		/*highボタンlowボタン表示*/
		$("#startB").hide();
		$("#title_label").hide();
		$("#highB").show();
		$("#lowB").show();
    });
	
	/*highボタン処理*/
	$("#highB").click(function(){
		var card = husecard();
		huseptn = card.huseptn;
		husenum = card.husenum;
		
		if(nownum > husenum){
			$("#issue").text('負け');
			$("#retryB").show();
		}else if(nownum===husenum){
			$("#issue").text('引き分け');
			$("#nextB").show();
		}else{
			victory += 1;
			$("#issue").text('勝ち');
			if(victory!=7){
				$("#nextB").show();
			}
		
			if(victory===3||victory===5||victory===7){
				$("#getB").show();
			}
		}
	});
	
	/*lowボタン処理*/
	$("#lowB").click(function(){
		var card = husecard();
		huseptn = card.huseptn
		husenum = card.husenum;
		
		if(nownum < husenum){
			$("#issue").text('負け');
			$("#retryB").show();
		}else if(nownum===husenum){
			$("#issue").text('引き分け');
			$("#nextB").show();
		}else{
			victory += 1;
			$("#issue").text('勝ち');
			if(victory!=7){
				$("#nextB").show();
			}
		
			if(victory===3||victory===5||victory===7){
				$("#getB").show();
			}
		}
	});
	
	/*retryボタン処理*/
	$("#retryB").click(function(){
		location.href = highandlowUrl;
	});
	
	/*nextボタン処理*/
	$("#nextB").click(function(){
		
		$("#issue").text('');
		
		$("#card1").attr('src', $("#card1").attr('src').replace(nowptn+nownum, huseptn+husenum));
		$("#card2").attr('src', $("#card2").attr('src').replace(huseptn+husenum ,'z02'));
		
		nowptn = huseptn;
		nownum = husenum;
		
		$("#nextB").hide();
		$("#getB").hide();
		$("#highB").show();
		$("#lowB").show();
	});
	
	/*coingetボタン処理*/
	$("#getB").click(function(){
		if((victory -= 3) === 0){victory = 1;}
		
		var csrf_token = $('input[name=csrfmiddlewaretoken]').val();
		$.post(coinaddUrl, {csrfmiddlewaretoken: csrf_token ,add_coin_num:victory})
		.done(function() {
			location.href=mainpicUrl;
		});
	});
	
	/*伏せカードの処理の関数*/
	function husecard() {
		var huseptn = pattern[Math.floor( Math.random() * 4 )];
		var husenum = Math.floor( Math.random() * 13)+1;
		
		if((nowptn===huseptn)&&(nownum===husenum)){
			huseptn = pattern[Math.floor( Math.random() * 4 )];
			husenum = Math.floor( Math.random() * 13)+1;
		}
		
		$("#card2").attr('src', $("#card2").attr('src').replace('z02', huseptn+husenum));
		/*ボタン隠す*/
		$("#highB").hide();
		$("#lowB").hide();
		
		return {huseptn:huseptn, husenum:husenum}
	}
}); 

