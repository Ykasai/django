$(function(){
	/*dialogの設定*/
	$("#dialog").dialog({
		autoOpen: false,
		modal: true,
		
		/*ボタンの表示と処理*/
		buttons: {
			"OK": function(){
				var zukanUrl= location.href;
				var resetUrl=zukanUrl.replace("zukan", "reset");
				
				var csrf_token = $('input[name=csrfmiddlewaretoken]').val();
				$.post( resetUrl, {csrfmiddlewaretoken: csrf_token})
				.done(function() {
					location.href=zukanUrl;
				});
			},
			"Cancel": function(){
				$(this).dialog('close');
			}
		}
	});
	
	/*初期化ボタンの処理*/
	$("#deleteB").click(function(){
		/*ダイアログの表示*/
		$('#dialog').dialog('open');
	});
});