# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='dropItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='名前', max_length=255)),
                ('comment', models.TextField(verbose_name='コメント', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='名前', max_length=255)),
                ('comment', models.TextField(verbose_name='コメント', blank=True)),
                ('num', models.IntegerField(verbose_name='出た回数', default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
