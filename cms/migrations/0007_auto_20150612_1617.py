# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0006_auto_20150610_1727'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='num',
            new_name='drop_num',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='pic',
            new_name='pic_name',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='prob',
            new_name='probability',
        ),
    ]
