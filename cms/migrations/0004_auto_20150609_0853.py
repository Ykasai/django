# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0003_item_prob'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='prob',
            field=models.IntegerField(verbose_name='確率(%)', default=0),
            preserve_default=True,
        ),
    ]
