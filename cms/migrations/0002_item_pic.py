# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='pic',
            field=models.CharField(verbose_name='画像名', blank=True, max_length=255, default=2),
            preserve_default=False,
        ),
    ]
