# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0002_item_pic'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='prob',
            field=models.IntegerField(default=0, verbose_name='確率'),
            preserve_default=True,
        ),
    ]
