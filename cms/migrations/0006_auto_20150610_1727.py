# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0005_auto_20150610_1700'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='User_date',
            new_name='User_data',
        ),
    ]
