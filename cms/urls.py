from django.conf.urls import patterns, url
from cms import views
from django.conf import settings

urlpatterns = patterns('',
    url(r'^gatya/drop/$', views.drop_item, name='drop_item'),
    url(r'^gatya/$', views.main_pic, name='main_pic'),
    url(r'^gatya/zukan/$', views.zukan, name='zukan'),
    url(r'^gatya/reset/$', views.drop_reset, name='reset'),
    url(r'^gatya/game/highandlow/$', views.high_and_low, name='high_and_low'),
    url(r'^gatya/coinadd/$', views.coin_add, name='coin_add'),

    (r'^static_site/(?P<path>.*)$', 'django.views.static.serve',
     {'document_root': settings.MEDIA_ROOT}),
    )
