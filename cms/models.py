from django.db import models


class Item(models.Model):
    '''アイテムのデータ'''
    name = models.CharField(u'名前', max_length=255)
    comment = models.TextField(u'コメント', blank=True)
    pic_name = models.CharField(u'画像名', max_length=255, blank=True)
    drop_num = models.IntegerField(u'出た回数', default=0)
    probability = models.IntegerField(u'確率(%)', default=0)

    def __str__(self):    # Python2: def __unicode__(self):
        return self.name


class User_data(models.Model):
    '''ユーザーデータ'''
    coins = models.IntegerField(u'コイン枚数', default=5)

    def __int__(self):    # Python2: def __unicode__(self):
        return self.coins
