from django.contrib import admin
from cms.models import Item, User_data


class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'comment', 'drop_num', 'probability')
    list_display_links = ('id', 'name',)
admin.site.register(Item, ItemAdmin)


class User_dataAdmin(admin.ModelAdmin):
    list_display = ('coins',)
    list_display_links = ('coins',)
admin.site.register(User_data, User_dataAdmin)
