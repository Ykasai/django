from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.db import transaction
from cms.models import *
import random


@transaction.commit_on_success
def drop_item(request):
    '''Itemの抽選'''
    rand = random.randint(0, 99)
    list = []
    all_item = Item.objects.all()

    '''ガチャの中身を入れる'''
    for item in all_item:
        i = item.probability
        while i > 0:
            list.append(item.id)
            i -= 1

    '''コインを投入'''
    coin_datas = User_data.objects.filter(id=1)
    for coin_data in coin_datas:
        if coin_data.coins > 0:
            coin_data.coins -= 1
            coin_data.save()

            '''ガチャる&出たアイテムのカウントアップ'''
            items = Item.objects.filter(id=list[rand])
            for item in items:
                item.drop_num += 1
                item.save()
            #渡すデータの設定
            data = dict(items=items)
        else:
            #渡すデータの設定
            data = dict()

    return render_to_response('cms/drop_item.html',
                               data,
                               context_instance=RequestContext(request))


def main_pic(request):
    '''初期ページ'''

    '''コイン枚数の呼び出し'''
    data_coins = User_data.objects.filter(id=1)
    for data_coin in data_coins:
        coin = data_coin.coins

    return render_to_response('cms/main_pic.html',  # 使用するテンプレート
                              dict(coinlist=range(0, coin), coinNum=coin),
                              context_instance=RequestContext(request))


def zukan(request):
    '''図鑑の表示'''
    sum = Item.objects.count()
    items = Item.objects.filter(drop_num__gt=0).order_by('id')

    return render_to_response('cms/zukan.html',  # 使用するテンプレート
                              dict({'items': items}, sum=sum),  # テンプレートに渡すデータ
                              context_instance=RequestContext(request))


@transaction.commit_on_success
def drop_reset(request):
    '''図鑑のリセット'''

    items = Item.objects.all()
    for item in items:
        item.drop_num = 0
        item.save()

    return redirect('cms:zukan')


def high_and_low(request):
    '''HighAndLowゲーム'''
    return render_to_response('cms/high_and_low.html',  # 使用するテンプレート
                              context_instance=RequestContext(request))


@transaction.commit_on_success
def coin_add(request):
    coin_datas = User_data.objects.filter(id=1)

    for coin_data in coin_datas:
        coin_data.coins += int(request.POST['add_coin_num']);

        if coin_data.coins > 5:
            coin_data.coins = 5

        coin_data.save()

    return redirect('cms:main_pic')
